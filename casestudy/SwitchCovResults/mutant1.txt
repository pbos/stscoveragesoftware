petra@petra-Latitude-E5450:~/data/SUMBAT/data-systems-and-complete-testing/stsinmaude$ python3 python/script.py /home/petra/Programs/Maude/Z3-Maude/maude-smt/maude.linux64 brp-sts.maude id5 '"msg1" |-> 0, "msg3" |-> 0, "msg2" |-> 0, "number" |-> 0, "toggle" |-> 0, "roundnr" |-> 0, "max_rn" |-> 5' 16 mutant1.jar
Nr of test cases: 6
Test case 0

Send input: IREQ_0_1_1
Received output: OFRAME_1_0_0_0
Send input: IACK
Received output: OFRAME_0_0_1_1
Send input: IACK
Received output: OFRAME_0_1_0_1
Send input: ITIMEOUT
Received output: OFRAME_0_1_0_1
Send input: ITIMEOUT
Received output: OFRAME_0_1_0_1
Send input: ITIMEOUT
Received output: OFRAME_0_1_0_1
Send input: ITIMEOUT
Received output: OFRAME_0_1_0_1
Send input: ITIMEOUT
Received output: OFRAME_0_1_0_1
Send input: ITIMEOUT
Received output: OCONF_2
PASS

Test case 1

Send input: IREQ_0_1_1
Received output: OFRAME_1_0_0_0
Send input: ITIMEOUT
Received output: OFRAME_1_0_0_0
Send input: ITIMEOUT
Received output: OFRAME_1_0_0_0
Send input: ITIMEOUT
Received output: OFRAME_1_0_0_0
Send input: ITIMEOUT
Received output: OFRAME_1_0_0_0
Send input: ITIMEOUT
Received output: OFRAME_1_0_0_0
Send input: ITIMEOUT
Received output: OCONF_0
PASS

Test case 2

Send input: IREQ_0_0_1
Received output: OFRAME_1_0_0_0
Send input: IACK
Received output: OFRAME_0_0_1_0
Send input: IACK
Received output: OFRAME_0_1_0_1
Send input: IACK
Received output: OCONF_1
PASS

Test case 3

Send input: IREQ_0_1_0
Received output: OFRAME_1_0_0_0
Send input: IREQ_1_0_1
Received output: OFRAME_1_0_0_1

FAIL: Output gate OFRAME was not expected!

Test case 4
Send input: ITIMEOUT
Received output: ONOK
PASS

Test case 5

Send input: IACK
PASS

petra@petra-Latitude-E5450:~/data/SUMBAT/data-systems-and-complete-testing/stsinmaude$
