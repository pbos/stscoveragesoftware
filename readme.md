# Requirements for running the software

- Your PC runs a Linux operating system. This software may possibly run on Windows systems, but no guarantees can be given.
- This repository has been cloned to your PC
- python3 has been installed on your PC
- You obtained the adapted Maude version with Z3 by emailing me (since I cannot distribute it publicly), and you placed the files in the directory ```Z3-Maude\maude-smt\```.


# Setup

We use a python3 script to:
- Call the language and tool set Maude with an STS specification in Maude language, and obtain test cases with switch coverage (a manual on the Maude language and tool set can be found at http://maude.lcc.uma.es/manual271/maude-manual.html).
- Execute the test cases, either automatically on a given implementation, or by obtaining the test outputs from the user via a command line interface.


# Case study STSes

The directory ```/casestudy/XML``` contains Symbolic Transition Systems (STS) in XML-format.
The text fields of the XML elements will be interpreted by the tool Maude, and hence may contains some Maude syntax (either predefined syntax from the Maude tool or added Maude declarations from ```/stsinmaude/stsLib.maude```).
The file ```adder.xml``` is the example STS from the paper. 
The file ```BRP.xml``` is the reference implementation of the BRP protocol as given on http://automata.cs.ru.nl/BenchmarkBRP/Description.


# Generating STS specifications in the Maude language

To generate an STS specification from an XML file (e.g. ```adder.xml```), execute the following commands in a terminal:

```
cd stsinmaude
python3 python/parseXmlSts.py ../casestudy/XML/adder.xml
```

The name of the generated file will be printed, e.g. ```adder-sts.maude```, which can be found in the directory ```stsinmaude```.


# Starting test execution using the python3 script

To start the execution run the following commands in a terminal:

```
cd stsinmaude
python3 python/script.py ../Z3-Maude/maude-smt/<Maude binary> <.maude file> <initial location>
 <variable initialization> <number of switches> [system under test]
```

- ```<Maude binary>```: choose a Maude binary appropriate for your system: ```maude.linux64```, ```maude.linux```, or ```maude.exe```
- ```<.maude file>```: give the name of the file generated in the previous step
- ```<initial location> <variable initialization> <number of switches>```: the values to insert here are stated in comments at the bottom of your```.maude``` file (use single quotes if values consist of spaces).

Hence, to execute test cases for ```adder-sts.maude``` on a Linux64 system, the command is:

```
python3 python/script.py ../Z3-Maude/maude-smt/maude.linux64 adder-sts.maude l0 '"X" |-> 0' 3
```

- ```[system under test]```: Optionally, a system under test can be provided. This currently only works for the BRP case study (as outputs from the system need to be parsed). One can choose ```ref.jar```, which is the implementation conforming to the specification, or one of the mutants ```mutant1.jar```, ```mutant2.jar```, ... , ```mutant6.jar```. These files can be found in the directory ```casestudy/BRP_JARS/max5```. Only the name of the file, and not this directory, must be given in the command.


# Test case execution

Once test execution has been started as described in the previous section, Maude generates the switch coverage test cases. This may take some time. Maude's results are stored, such that the next time test execution is started, the results can be read from file.

When all test cases have been generated, the number of test cases is shown. To execute a test case, press Enter in the terminal. If an output is not as expected according to the test case, test execution of the current test case immediately stops with a fail message. Otherwise the test case ends with PASS.

If a system under test was given in the command for starting test execution, the test cases are executed automatically (after starting them with Enter). The inputs and outputs of the test case are then printed.

If no system under test was given in the command for starting test execution, test cases are executed in manual mode. This means that input gates and their parameter values of the test case are generated, while output gates and their parameter values have to be typed in the terminal.
Also, the expected gates of the test cases are shown. If an output is required according to the test case, the user is first asked to enter the output gate, and after that, to enter the parameter values.

Here is an example of test case execution:

```
cd stsinmaude
python3 python/script.py ../Z3-Maude/maude-smt/maude.linux64 adder-sts.maude l0 '"X" |-> 0' 3
Nr of test cases: 1
Test case 0: ['inX', 'outX', 'inX', 'outX', 'done']

Send input: inX 6
Give output gate: outX
Give associated gate value or type None: 6
Received output: outX 6
Send input: inX 10
Give output gate: done
Give associated gate value or type None: None
Received output: done
FAIL: Output gate done was not expected!
```
