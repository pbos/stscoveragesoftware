		     \||||||||||||||||||/
		   --- Welcome to Maude ---
		     /||||||||||||||||||\
	    Maude alpha96b built: Nov 15 2012 16:54:51
	    Copyright 1997-2011 SRI International
		   Fri Nov  9 15:04:52 2018
Maude> Maude> ==========================================
search in BRP-STS : < id5 ; nul ; 0 ; "msg1" |-> 0, "msg3" |-> 0, "msg2" |-> 0,
    "number" |-> 0, "toggle" |-> 0, "max_rn" |-> 5, "roundnr" |-> 0 ; nul > =>*
    < l:Loc ; t12 @ Lt ; C ; V ; L > .

Solution 1 (state 74)
states: 75  rewrites: 121266 in 580ms cpu (580ms real) (209079 rewrites/second)
l:Loc --> id4
Lt --> t5 @ t11 @ t5 @ t13 @ t15
C --> 15
V --> "max_rn" |-> 5, "msg1" |-> var0:Bool, "msg2" |-> var1:Bool, "msg3" |->
    var2:Bool, "number" |-> 2, "roundnr" |-> 1, "toggle" |-> 0
L --> toQTup("IREQ", true, 'var0:Bool @ 'var1:Bool @ 'var2:Bool, maybeT) @
    toQTup("OFRAME", false, 'var3:Int @ 'var4:Int @ 'var5:Int @ 'var6:Bool,
    '_and_['_=i=_['0.Zero,'0.Zero],'_=i=_['var3:Int,'s_['0.Zero]],'_=i=_[
    'var4:Int,'0.Zero],'_=i=_['var5:Int,'0.Zero],'_=b=_['var6:Bool,
    'var0:Bool]]) @ toQTup("IACK", true, nul, 'true.Bool) @ toQTup("OFRAME",
    false, 'var7:Int @ 'var8:Int @ 'var9:Int @ 'var10:Bool, '_and_['_=i=_[
    'var7:Int,'0.Zero],'_=i=_['var8:Int,'0.Zero],'_=i=_['var9:Int,'s_[
    '0.Zero]],'_=i=_['s_['0.Zero],'s_['0.Zero]],'_=b=_['var10:Bool,
    'var1:Bool]]) @ toQTup("IACK", true, nul, 'true.Bool) @ toQTup("OFRAME",
    false, 'var11:Int @ 'var12:Int @ 'var13:Int @ 'var14:Bool, '_and_['_=i=_[
    'var11:Int,'0.Zero],'_=i=_['var12:Int,'s_['0.Zero]],'_=i=_['var13:Int,
    '0.Zero],'_=i=_['s_^2['0.Zero],'s_^2['0.Zero]],'_=b=_['var14:Bool,
    'var2:Bool]])
Maude> Bye.
