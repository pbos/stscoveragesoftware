import matplotlib.pyplot as plt
import sys
import statistics as st

res = []
for i in range(100):
	with open('txsout' + str(i)) as f:
		data = f.read()
		startIndex = data.rfind('..') + 2
		endIndex = data[startIndex:].find(':')
		res.append(int(data[startIndex:startIndex+endIndex]))

res.sort()
strres = 'result list: ' + str(res) + '\nmean: ' + str(st.mean(res)) + '\nmedian: ' + str(st.median(res)) + '\nstandard deviation: ' + str(st.stdev(res))
print(strres)
store = open('resultlist', 'w')
store.write(strres)
store.close()

plt.hist(res,bins=range(min(res),max(res)+1), align='left', rwidth=1)
plt.xticks(range(0,max(res)+5,50) )
plt.yticks(range(0, 10, 2))
plt.show()
