class SUT:
	
	def sendInput(self, gate, var):
		if gate != 'reset':
			print('Send input: ' + gate + ' ' + ('' if var == [] else var[0][1])) 
	
	def getOutput(self):
		gate = input('Give output gate: ')
		var = input('Give associated gate value or type None: ')
		if var == 'None':
			print('Received output: ' + gate)
			return (gate,[])
		else:
			print('Received output: ' + gate + ' ' + var)
			return (gate,[var])

	def endConnection(self):
		pass
