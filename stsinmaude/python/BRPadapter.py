import socket
import subprocess
import sys
import time

class BRPSUT:
	
	proc = None
	sock = None
	
	def __init__(self,imp):
		self.proc = subprocess.Popen(['java', '-jar','../casestudy/BRP_JARS/max5/' + imp], stdout=subprocess.PIPE,stderr=subprocess.PIPE)
		# stdout_value, stderr_value = self.proc.communicate()
		# print('found:' + stdout_value.decode() + ' end')
		time.sleep(0.5)
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.sock.connect(('localhost', self.getPortNr(imp)))
		
	def getPortNr(self, imp):
		if imp == 'ref.jar':
			return 7890
		elif imp == 'mutant1.jar':
			return 7891
		elif imp == 'mutant2.jar':
			return 7892
		elif imp == 'mutant3.jar':
			return 7893
		elif imp == 'mutant4.jar':
			return 7894
		elif imp == 'mutant5.jar':
			return 7895
		elif imp == 'mutant6.jar':
			return 7896
		else:
			raise RuntimeError('Unexpected implementation!')

	def getInputEncoding(self, gate, varList):
		if gate == 'reset':
			if varList:
				# time.sleep(0.5)
				self.sock.recv(1024).decode()
			return 'reset'
		else:
			for varName,val in varList:
				gate = gate + '_' + ('0' if val == 'false' else ('1' if val == 'true' else val))
			print('Send input: ' + gate)
			return gate

	def sendInput(self, gate, var):
		self.sock.send((self.getInputEncoding(gate,var) + '\n').encode(sys.stdout.encoding))

	def getOutput(self):
		outp = self.sock.recv(1024).decode()
		print('Received output: ' + outp[:-1])
		parts = outp[:-1].split('_')
		if parts[0] == 'OFRAME':
			return (parts[0], parts[1:-1] + [('false' if parts[-1] == '0' else 'true')])
		else:
			return (parts[0],parts[1:])
		
	def endConnection(self):
		self.sock.close()
		self.proc.terminate()
