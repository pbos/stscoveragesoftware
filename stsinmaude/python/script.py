from SUT import SUT
from BRPadapter import BRPSUT
import ast
import subprocess
import sys
from pathlib import Path
import random

def main(argv):
	maude = argv[0]
	stsFile = argv[1]
	initialState = '< ' + argv[2] + ' ; nul ; 0 ; ' + argv[3] + ' ; nul >'
	sols = findTestCases(maude, stsFile, initialState, argv[4])
	condition = True
	# i = 1
	# while condition:
	condition = executeTestCases(maude, stsFile, sols, initialState, SUT() if len(argv) < 6 else BRPSUT(argv[5]), len(argv) < 6)
		# print('end test run ' + str(i))
		# i = i+1
	# with open("mutant6statistics.txt", "a") as myfile:
		# myfile.write(str(i*47+6) + ', ')
	
def findTestCases(maude,stsFile,initialState,nrTrans):
	transLists = []
	transSolDict = []
	for i in range(int(nrTrans)):
		res = 'No solution.\n\n'
		myFile = Path(stsFile[:-6] + '/t' + str(i) + '.txt')
		if myFile.is_file():
			res = myFile.read_text()
		else:
			myDir = Path(stsFile[:-6])
			if not(myDir.exists()):
				myDir.mkdir()
			query = 'search [1] ' + initialState + ' =>* < l:Loc ; t' + str(i)  + ' @ Lt ; C ; V ; L > .'
			res = queryMaude(maude, stsFile, query)
			myFile.write_text(res)
		if res.splitlines()[-3] == 'No solution.':
			raise RuntimeError('Transition t' + str(i) + ' could not be reached!')
		else:
			trans = getTransList(res,i)
			transLists.append(trans)
			transSolDict.append((trans,res))
	selection = deletePrefixPaths(transLists,1)
	res = [(t,ast.literal_eval(getZ3StringFromMaudeSolution(s, maude))) for (t,s) in transSolDict if t in selection]
	res.sort(key=lambda tup: len(tup[0]), reverse=True)
	return res
	
def executeTestCases(maude, stsFile, sols, initialState, sut, printTestCase): #returns hasPassed
	print('Nr of test cases: ' + str(len(sols)))
	for i,v in enumerate(sols):
		if printTestCase:
			print('Test case ' + str(i) + ': ' + str([el[1] for el in v[1]]))
		else:
			print('Test case ' + str(i))
		input()
		res = executeTest(maude, stsFile, sols[i][1], sut, initialState) + '\n'
		print(res)
		sut.sendInput('reset', v[1][-1][2])
		if res[0:4] == 'FAIL':
			return False
	return True

def getAssert(asserts, inpValues):
	for (varName,value) in inpValues:
		asserts = asserts + '(assert (= ' + varName + ' ' + value +  ')) '
	return asserts
	
def executeTest(maude, stsFile, path, sut, initialState):	
	trace = []
	asserts = ''
	for (z3assertstr,gate,isInp,varList) in path:
		varName = None if varList == [] else varList[0][0]
		varIsBool = None if varList == [] else varList[0][1]
		if isInp:
			if varList == []:
				# print('Send input: ' + gate)
				trace.append((gate,isInp,[]))
				sut.sendInput(gate, [])
			else:
				inpValues = [(varName,('false' if random.randint(0,1) == 0 else 'true') if varIsBool else str(random.randint(-sys.maxsize-1,sys.maxsize))) for (varName,varIsBool) in varList]
				Z3satresponse = getZ3Response(z3assertstr, varName, getAssert(asserts,inpValues),False)
				if not parseZ3satOutput(Z3satresponse):
					Z3response = getZ3Response(z3assertstr, varList, asserts,True)
					inpValues = parseZ3getValueOutput(Z3response,varList)
				asserts = getAssert(asserts, inpValues)
				# print('Send input: ' + gate + ' ' + str(inpValues) )
				trace.append((gate,isInp,inpValues))
				sut.sendInput(gate, inpValues)
		else:
			(recLabel,recValues) = sut.getOutput()
			recVarValues = list(zip([varName for (varName, isBool) in varList], recValues))
			if recLabel != gate:
				trace.append((recLabel,False,recVarValues))
				# response = queryMaude(maude,stsFile,getTraceQuery(trace, initialState))
				if(checkAllSolutionsForTrace(maude, stsFile, trace, initialState)):
					return 'FAIL: Output gate ' + recLabel + ' was not expected!'  # + ' (' + str(len(trace)) + ' steps)' # + '\nFull trace:\n' + getFailedTraceStr(trace)
				else:
					return 'INCONCLUSIVE: Output gate not in path.\nExpected output: ' + gate + '\nReceived output: ' + recLabel # + '\nFull trace:\n' + getFailedTraceStr(trace)
				break #not needed?
			elif varList == []:
				# print('Received output: ' + recLabel)
				trace.append((gate,isInp,[]))
			elif recValues == []: #TODO: check length ipv only empty
				return 'FAIL: Output gate: ' + gate + 'was expected to have an associated value'
			else: 
				# print('Received output: ' + recLabel + ' ' + str(recValues))
				asserts = getAssert(asserts, recVarValues)
				Z3response = getZ3Response(z3assertstr, varList, asserts,False)
				if not parseZ3satOutput(Z3response):
					trace.append((recLabel,False,recVarValues))
					if(checkAllSolutionsForTrace(maude, stsFile, trace, initialState)):
						return 'FAIL: values ' + str(recValues) + ' were not expected for output gate ' + gate # + ' (' + str(len(trace)) + ' steps)' # + '!\nFull trace:\n' + getFailedTraceStr(trace)
					else:
						return 'INCONCLUSIVE: Failed to follow path. Received values ' + str(recValues) + ' for output gate ' + gate # + '\nFull trace:\n' + getFailedTraceStr(trace)
				else:
					trace.append((gate,isInp,recVarValues))
	return 'PASS'
	
def deletePrefixPaths(paths,i):
	if len(paths) <= 1:
		return paths
	elif len(paths) <= i:
		res = deletePrefixPaths(paths[1:],1)
		res.append(paths[0])
		return res
	else:
		res = comparePaths(paths[0], paths[i])
		if res == 1:
			return deletePrefixPaths(paths[1:],1)
		elif res == 0:
			del paths[i]
			return deletePrefixPaths(paths,i)
		else:
			return deletePrefixPaths(paths,i+1)			

def getTransList(sol,i):
	startIndex = sol.find('Lt -->')
	endIndex = sol.find('C -->')
	transL = sol[startIndex+7:endIndex-1].replace('\n   ','').split(' @ ')
	t = 't' + str(i)
	if(transL == ['(nul).List{Trans}']):
		return [t]
	else:
		transL.reverse()
		transL.append(t)
		return transL

def comparePaths(path0, path1): # -1: no match; 0: keep path0; 1: keep path1
	if len(path0) >= len(path1):
		return 0 if path0[:len(path1)] == path1 else -1
	else:
		return 1 if path1[:len(path0)] == path0 else -1
	
def getZ3StringFromMaudeSolution(maudeReply, maude):
	startIndex = maudeReply.find('L --> ')
	endIndex = maudeReply.rfind(')')
	maudeReply = getZ3ListFromQTup(maudeReply[startIndex+6:endIndex+1], maude)
	return maudeReply[maudeReply.find('"')+1:maudeReply.rfind('"')]
	
def getZ3ListFromQTup(QTupStr, maude):
	Path('calc.maude').write_text(
		'load stsLib\n\n' +
		'mod CALC is\n' +
			'\tpr STS-LIB .\n' +
			'\top calc : -> String .\n' +
			'\teq calc = getAllSmtSubstrs(' + QTupStr + ') .\n\nendm')
			# '\teq calc = toPython(tail(toSmtLib(' + QTupStr + '))) .\n\nendm')
	return queryMaude(maude,'calc.maude','rew calc .')
	

def queryMaude(maude, stsFile, query):
	proc = subprocess.Popen([maude],
							stdin=subprocess.PIPE,
							stdout=subprocess.PIPE,
							)
	return proc.communicate(('load ' + stsFile + '\n' + query).encode(sys.stdout.encoding))[0].decode()

def checkAllSolutionsForTrace(maude, stsFile, trace, initialState): #returns hasFailed
	return checkSolutionForTrace(maude, stsFile, trace, initialState, "", 0)
		
def checkSolutionForTrace(maude, stsFile, trace, state, asserts,i): #returns hasFailed
	if len(trace) == 0:
		return False
	else:
		(gate,isInp,values) = trace[0]
		query = 'search ' + state + ' =>1 St:State such that checkGateAndVal(St:State,"' + gate + '",' + str(isInp).lower() + ',' + toValList(values) + ',"' + asserts + '") .'
		# print('QUERY' + query)
		response = queryMaude(maude,stsFile,query)
		# print('RESP' + response)
		asserts = getAssert(asserts,values)
		for sol in response.split('\n\n')[1:-1]:
			if not(checkSolutionForTrace(maude, stsFile, trace[1:],sol[sol.find('St:State --> ')+13:],asserts,i+1)):
				return False
		return True
		
def toValList(values):
	res = ''
	for (varName, value) in values:
		res = res + value + ' @ '
	return res + 'nul'

def parseZ3satOutput(response):
	return response.splitlines()[0] == 'sat'

def parseZ3getValueOutput(response,varList):
	if response[:3] != "sat":
		raise RuntimeError('Cannot get value from unsatisfiable formula!')
	else:
		valStrs = response[4:-2].splitlines()
		return [(varName, valStrs[i][3+len(varName):-1]) for (i,(varName,isBool)) in enumerate(varList) ]

def getGetValue(varList):
	res = '(get-value ('
	for (varName,isBool) in varList:
		res = res + varName + ' '
	return res + '))'

def getZ3Response(z3assertstr, varList,asserts,getValue):
	if varList == []:
		raise RuntimeError('No Z3 query needed!')
	else:
		file = open('z3code.txt','w')
		z3str = z3assertstr + asserts + ' (check-sat) ' + (getGetValue(varList) if getValue else '') + '(exit)'
		# print('to Z3: ' + z3str)
		file.write(z3str)
		file.close()
		proc1 = subprocess.Popen(['z3', 'z3code.txt', 'smt2'],
						stdout=subprocess.PIPE,
						stderr=subprocess.PIPE
						)
		stdout_value, stderr_value = proc1.communicate()
		return stdout_value.decode()
		
# def getTraceQuery(trace, initialState):
#	res = ''
#	i = 0
#	for (gate,isInp,val) in trace:
#		res = 'toQTup("' + gate + '", ' + ('true' if isInp else 'false') + ',I' + str(i) + ':Maybe{Variable},T' + str(i) + ':Maybe{Term}) ' + res
#		i = i+1
#	return 'search [,' + str(len(trace)) + '] ' + initialState + ' =>* < l:Loc ; T1:Trans ; C ; V ; ' + res + ' > .'
	
def parseMaudeSolutions(response, maude):
	return [getZ3StringFromMaudeSolution(s, maude) for s in response.split('\n\n')[1:-1]]

def getFailedTraceStr(trace):
	res = ''
	for (gate,isInp,val) in trace:
		res = res + gate + ('? ' if isInp else '! ') + ('' if val is None else val) + '\n'
	return res[:-1]
	
if __name__ == "__main__":
    main(sys.argv[1:])
    
# /home/petra/Programs/Maude/Z3-Maude/maude-smt/maude.linux64
