import xml.etree.ElementTree as ET
import sys

def main(argv):
	print(parseFromXML(argv[0]))

def parseFromXML(filePath):
	tree = ET.parse(filePath)
	root = tree.getroot()
	name = root.get('name')
	gates = parseAlphabet(root.find('alphabet'))
	locVars = parseLocationVars(root.find('globals'))
	(locs,init) = parseLocations(root.find('locations'))
	trans = parseTransitions(root.find('transitions'))
	(transrules, params) = getTransRules(trans, locVars, gates)
	fName = writeStsToFile(name,
				getHeader(name) + 
				getLocDecls(locs) + 
				getTransDecls(len(trans)) + 
				getVarDecls(params) +
				transrules +
				getEnd(),
				init,
				getInitV(locVars),
				len(trans))
	return fName

def writeStsToFile(name, stsStr, initState, initLocVars, nrTrans):
	fName = name.lower() + '.maude'
	file = open(fName,'w')
	file.write(stsStr + '\n--- ' + initState + '\n--- ' + initLocVars + '\n--- ' + str(nrTrans) + '\n')
	file.close()
	return fName

def parseAlphabet(alphabet):
	inputs = parseSymbols(alphabet.find('inputs').findall('symbol'), True)
	outputs = parseSymbols(alphabet.find('outputs').findall('symbol'), False)
	outputs.update(inputs)
	return outputs

def parseSymbols(symbols, isInput):
	alf = dict()
	for sym in symbols:
		name = sym.get('name')
		params = sym.findall('param')
		alf[name] = ('true' if isInput else 'false', [] if params == [] else [param.get('type') for param in params ])
	return alf

def parseLocationVars(globalV):
	locVars = dict()
	for var in globalV:
		locVars[var.get('name')] = (var.get('type'), None if var.text is None else var.text)
	return locVars

def parseLocations(locations):
	init = None
	locs = set()
	for loc in locations:
		l = loc.get('name')
		locs.add(l)
		if(not(loc.get('initial') is None)):
			if init is None:
				init = l
			else:
				raise RuntimeError('Only one location can be the initial one!')
	if init is None:
		raise RuntimeError('No initial location!')
	return (locs,init)

def parseTransitions(transitions):
	return [{'from' : t.get('from'),
			 'to': t.get('to'),
			 'gate' : t.get('symbol'),
			 'param' : [] if t.get('params') is None else t.get('params').split(","),
			 'guards' : [] if t.find('guards') is None else [ g.text for g in t.find('guards')],
			 'assgn' : None if t.find('assignments') is None else [ (a.get('to'), a.text) for a in t.find('assignments')]  
				} for t in transitions]

def getHeader(name):
	return 'load stsLib\n\nmod ' + name + ' is\n\tpr STS-LIB .\n'

def getVarDecls(params):
	intp = set()
	boolp = set()
	res = ''
	if len(params) > 0:
		for (p,t) in params:
			if t == 'Int':
				intp.add(p)
			elif t == 'Bool':
				boolp.add(p)
			else:
				raise RuntimeError(t + ' is no Int or Bool!')
		res = '\tvars '
		for p in intp:
			res = res + p + ' '
		if len(intp) > 0:
			res = res + ': Int .\n'
		if len(intp) > 0 and len(boolp) > 0:
			res = res + '\tvars '
		for p in boolp:
			res = res + p + ' '
		if len(boolp) > 0:
			res = res + ': Bool .\n'
	return (res + 
			'\tvars V V\' : Map{String,EitherBoolInt} .\n' + 
			'\tvar C : Nat .\n' + 
			'\tvars L L\' : List{QuanTup} .\n' + 
			'\tvar Lt : List{Trans} .\n')

def getInitV(locVars):
	res = ''
	for var, val in locVars.items():
		if(not(val[1] is None)):
			res = res + '"' + var + '" |-> ' + val[1] + ', '
	if res == '':
		return 'empty'
	else:
		return res[:-2]

def getLocDecls(locVars):
	res = '\tops '
	for loc in locVars:
		res = res + loc + ' '
	return res + ': -> Loc .\n'

def getTransDecls(nr):
	res = '\tops '
	for i in range(nr):
		res = res + 't' + str(i) + ' '
	return res + ': -> Trans .\n'
	
def getTransRules(trans, locVars, gates):
	res = ''
	i = 0
	params = set()
	for t in trans:
		(ruleStr, p) = getTransRule(t,i, locVars, gates)
		res = res + ruleStr
		params.update(p)
		i = i + 1
	return (res, params)

def updateLocVars(assignments, locVars):
	res = 'V'
	updated = set()
	for (to,val) in assignments:
		(val,upd) = checkValForLocVar(val, locVars)
		res = 'insert("' + to + '", ' + val + ', ' + res + ')'
		updated.update(upd)
	return (res, updated)

def checkValForLocVar(val, locVars):
	readVars = set()
	for l in locVars:
		old = val
		val = val.replace(l,'V["' + l + '"]')
		if(old != val):
			readVars.add(l)
	return (val, readVars)
	
def getGuardExpr(guards, locVars):
	res = ''
	readVars = set()
	for g in guards:
		(val, read) = checkValForLocVar(g, locVars)
		readVars.update(read)
		res = res + val + ' and '
	return (res[:-5], readVars)
	
def getQTup(trans, gates, locVars):
	qTupPref = ('toQTup("' + trans['gate'] + '", ' + 
		gates[trans['gate']][0] + ', ' + 
		getIVar(gates[trans['gate']], trans['param']) + ', ')
	if trans['guards'] == []:
		return (qTupPref + 'maybeT)', set())
	else:
		g = getGuardExpr(trans['guards'], locVars)
		return (qTupPref + 'upTerm(' + g[0] + '))',g[1])

def getIVar(gate, params):
	res = ''
	if not(gate[1] is None):
		for i,typeName in enumerate(gate[1]):
			if typeName == 'Int' or typeName == 'Bool':
				res = res + 'upTerm(' + params[i] + ') @ '
			else:
				raise RuntimeError(gate[1][i] + ' is no gate type!')
	return res + 'nul'
		
def getFreshVar(gate, params):
	if len(gate[1]) != len(params):
		raise RuntimeError('length not the same: ' + str(gate) + ' ; ' + str(params))
	else:
		res = ''
		for i in range(len(params)):
			if gate[1][i] == 'Int' or gate[1][i] == 'Bool':
				res = res + params[i] + ' := fresh' + gate[1][i] + 'Var(C + ' + str(i) + ') /\ \n\t\t\t\t'
			else:
				raise RuntimeError('Gate type ' + gate[1][i] + ' is not Int or Bool!')
		return res

def getHasMapping(updated, locVars):
	res = ''
	for var in updated:
		res = res + '\t\t\t\t$hasMapping(V, "' + var + '") /\ \n'
	return res

def getTransRule(trans,i, locVars, gates):		
	(str1,upd1) = (('V',set()) if trans['assgn'] is None else updateLocVars(trans['assgn'],locVars))
	(str2,upd2) = getQTup(trans, gates, locVars)
	param = set()
	if trans['param'] != []:
		pair = (trans['param'][0], (gates[trans['gate']][1])[0])
		param = set()
		param.add(pair)
	return ('\tcrl < ' + trans['from'] + ' ; Lt ; C ; V ; L > => ' +
			'< ' + trans['to'] + ' ; t' + str(i) + ' @ Lt ; C + ' + str(len(trans['param'])) + ' ; V\' ; L\' >\n' + 
			'\t\t\tif  ' +
			('' if gates[trans['gate']][1] == None else getFreshVar(gates[trans['gate']], trans['param'])) +
			'V\' := ' + str1 + ' /\ \n' +
			'\t\t\t\tL\' := L @ ' + str2 + ' /\ \n' + #concat
			getHasMapping(upd1.union(upd2), locVars) +
			'\t\t\t\tsmtCall(toSmtLibStr(L\')) =/= "unsat" .\n', 
				(set(zip(trans['param'],gates[trans['gate']][1]))))

def getEnd():
	return 'endm\n'

if __name__ == "__main__":
	main(sys.argv[1:])
