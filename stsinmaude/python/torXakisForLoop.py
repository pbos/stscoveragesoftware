import subprocess
import time
import sys

def main():
	sutProc = subprocess.Popen(['java', '-jar','../casestudy/BRP_JARS/max5/mutant6.jar'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	for i in range(100):
		outfile = open('txsout' + str(i),'w')
		txsProc = subprocess.Popen(['torxakis', 'specBRP.txs'], stdin=subprocess.PIPE,stdout=outfile,stderr=subprocess.STDOUT)
		txsProc.communicate('tester Model Sut\ntest 10000\nq\n'.encode(sys.stdout.encoding))
		print('exp ' + str(i))

if __name__ == "__main__":
    main()
